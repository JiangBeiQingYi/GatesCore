# 时空之门前端代码生成器

### 简介
时空之门前端代码生成器，是第四代动词算子式代码生成器，经过彻底的重构的先进动词算子式代码生成器，也是专用的Vue+ElementUI前端代码生成器，可以和多种后端代码生成器搭配。和平之翼和光对前端代码生成的支持是通过引用不同的时空之门的版本实现的。

本代码生成器的代码生成物基于开源软件Vue-Element-Admin,感谢原作者的贡献。至此，动词算子式代码生成器阵列中所有自制的组件都已经开源，最大限度的利于大家的学习。

这个代码生成器其实是完整代码生成器的前端代码生成组件，使用时是和和平之翼代码生成器或者第三代动词算子式代码生成器：光配合使用的。您可以下这些代码生成器使用，其中的前端项目其实是时空之门代码生成器生成的。这时，时空之门的jar包是放在宿主代码生成器的lib里的。‘

时空之门前端代码生成器支持独立的Swing界面，可以独立运行。

### 研发进展

近日释出了时空之门前端代码生成器Swing独立版 3.1.0 尝鲜版。此版本支持API的Controller约定。

时空之门前端代码生成器3.1.0 独立版尝鲜版已可以在本站附件处下载。此版本需要装好Java，双击即可运行，是Swing界面的。下载地址：
[https://gitee.com/jerryshensjf/GatesCore/attach_files](https://gitee.com/jerryshensjf/GatesCore/attach_files)    

#### 时空之门独立版Excel代码生成界面截图
![输入图片说明](https://images.gitee.com/uploads/images/2019/1216/100857_3e6ec20f_1203742.png "GatesCore_Full_3_1_0_Excel.png")

#### 时空之门独立版SGS代码生成界面截图
![输入图片说明](https://images.gitee.com/uploads/images/2019/1122/171348_59117bbf_1203742.png "GatesCore_Full_3_0_0_SGS.png")

### 项目图片

#### 时空之门
![输入图片说明](https://images.gitee.com/uploads/images/2019/0610/220701_41a056e0_1203742.jpeg "wormhole.jpg")

### 动词算子的力量

向Lisp和Lambda算子致敬

愿动词算子的力量与你同在

![输入图片说明](https://images.gitee.com/uploads/images/2019/0109/211947_432567db_1203742.png "magic_v.png")

代码生成物截图：
登录：
![登录](https://images.gitee.com/uploads/images/2019/0415/214758_8c47b686_1203742.png "vue_login.png")

Grid:
![Grid](https://images.gitee.com/uploads/images/2019/0415/214815_c2dfdd1e_1203742.png "vue_bonuses.png")

多对多：
![多对多](https://images.gitee.com/uploads/images/2019/0415/220549_b19d2ca4_1203742.png "Vue_mtm.png")

编辑，下拉列表：
![输入图片说明](https://images.gitee.com/uploads/images/2019/0416/085420_45584d04_1203742.png "vue_update_dropdown.png")

### 软件架构
ElementUI, Vue, JSON 基于开源软件vue-element-admin的架构

### 交流QQ群
无垠式代码生成器群  277689737

