package com.javaforever.gatescore.gui;

/**
 * This program is written by Jerry Shen(Shen Ji Feng) use the technology of
 * SWING GUI and the OO design
 * 
 * @author Jerry Shen all rights reserved.
 * Email:jerry_shen_sjf@qq.com
 * Please report bug to this email.
 * Open source under GPLv2
 * 
 * version 0.7.12
 */

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

@SuppressWarnings("serial")
class AboutFrame extends JFrame implements MouseListener {
	private JPanel aboutPane;

	private JLabel msg;
	
	private JLabel msg0;

	private JLabel msg1;

	private JLabel msg2;
	
	private JLabel msg3;

	private JButton exit;

	public AboutFrame(String strName) throws Exception {
		super(strName);
		setSize(280, 220);
		setResizable(false);
		String windows="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
		UIManager.setLookAndFeel(windows);
		this.dispatchEvent(new WindowEvent(this,WindowEvent.WINDOW_CLOSING));

		aboutPane = new JPanel();
		msg = new JLabel("    时空之门前端代码生成器    ");
		msg0 = new JLabel("  作者：沈戟峰 jerry_shen_sjf@qq.com");
		msg1 = new JLabel("             希望您喜欢！                       ");
		msg2 = new JLabel("    版本：3.0                       ");
		msg3 = new JLabel("    发布日期: 2019-11-18            ");
		exit = new JButton("关闭");
		exit.addMouseListener(this);
		aboutPane.add(msg);
		aboutPane.add(msg0);
		aboutPane.add(msg1);
		aboutPane.add(msg2);
		aboutPane.add(msg3);
		aboutPane.add(exit);

		setContentPane(aboutPane);
		setLocationRelativeTo(null);//窗口在屏幕中间显示
		
	}

	// the event handle to deal with the mouse click
	public void mouseClicked(MouseEvent e) {
		this.setVisible(false);
	}

	public void mousePressed(MouseEvent e) {
		//System.out.println("Jerry Press");

	}

	public void mouseReleased(MouseEvent e) {
		//System.out.println("Jerry Release");
	}

	public void mouseExited(MouseEvent e) {
		//System.out.println("Jerry Exited");

	}

	public void mouseEntered(MouseEvent e) {
		//System.out.println("Jerry Entered");

	}

	public static void main(String[] args) throws Exception{
		AboutFrame about = new AboutFrame("About");
		about.setVisible(true);
	}
}
