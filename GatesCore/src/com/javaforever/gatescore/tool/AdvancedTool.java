package com.javaforever.gatescore.tool;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;

import org.javaforever.gatescore.core.PluralUtil;
import org.javaforever.gatescore.utils.StringUtil;

public class AdvancedTool  extends JFrame implements MouseListener, ActionListener {
	private static final long serialVersionUID = 1L;
	protected JTextArea source;
	protected JTextArea target;
	protected JTextField first;
	protected JTextField serial;
	protected JTextField domainName;
	protected JTextField end;
	protected JButton generateBtn;
	protected JScrollPane sourcePane;
	protected JScrollPane targetPane;
	protected JCheckBox completeMethod;
	
	public AdvancedTool() throws Exception{
		super("无垠式代码生成器模板助手");
		
//		String windows="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
//		UIManager.setLookAndFeel(windows);		
//		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel pPanel = new JPanel();
		BorderLayout layout = new BorderLayout();
		pPanel.setLayout(layout);	
		
		JPanel jpl = new JPanel();
		Font myFont = new Font("wqy", Font.PLAIN, 20);
		domainName = new JTextField("domainName",12);
		first = new JTextField("sList.add(new Statement(",28);
		serial = new JTextField("1000",8);
		//afterSerial = new JTextField("L,0,\"",15);
		end =  new JTextField("\"));",10);
		completeMethod = new JCheckBox("生成完整方法");
		
		domainName.setFont(myFont);
		first.setFont(myFont);
		serial.setFont(myFont);
		//afterSerial.setFont(myFont);
		end.setFont(myFont);
		jpl.add(domainName);
		jpl.add(first);
		jpl.add(serial);
		//jpl.add(afterSerial);
		jpl.add(end);
		jpl.add(completeMethod);
		
		generateBtn = new JButton("Generate");
		generateBtn.addActionListener(this);
		jpl.add(generateBtn);
		
		jpl.setSize(1600,100);		
		
		JPanel contentPane = new JPanel();
		source = new JTextArea(200,60);
		Font sfont=new Font("宋体",Font.PLAIN,14); 
		//source.setFont(sfont);
		sourcePane = new JScrollPane(source,  JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		sourcePane.setSize(700,650);
		sourcePane.setVisible(true);
		target = new JTextArea(200,60);
		//target.setFont(sfont);
		targetPane = new JScrollPane(target,  JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		targetPane.setSize(700,650);
		targetPane.setVisible(true);
		contentPane.add(sourcePane);
		contentPane.add(targetPane);
		contentPane.setSize(1600,680);

		
		layout.addLayoutComponent(jpl, BorderLayout.NORTH);
		pPanel.add(jpl);
		layout.addLayoutComponent(contentPane, BorderLayout.CENTER);
		pPanel.add(contentPane);
		this.setContentPane(pPanel);
		setSize(1600, 800);
		setLocationRelativeTo(null);//窗口在屏幕中间显示
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if (e.getSource() == generateBtn) {

				String [] statements = source.getText().split("\n");
				Long serialInt = Long.valueOf(serial.getText());
				String capDomainName = StringUtil.capFirst(domainName.getText().trim());
				String lowerDomainName = StringUtil.lowerFirst(capDomainName);
				String capPlural = StringUtil.capFirst(PluralUtil.lookupPlural(capDomainName));
				String lowerPlural = StringUtil.lowerFirst(capPlural);
				StringBuilder sb = new StringBuilder();
				if (completeMethod.isSelected()) {
					sb.append("FrontMethod method = new FrontMethod();\n")
						.append("method.setStandardName(\"add\"+StringUtil.capFirst(this.domain.getStandardName()));\n")
						.append("List<Writeable> sList = new ArrayList<Writeable>();\n");
				}
				for (String s:statements) {	
					s=s.replaceAll("\"", "\\\\\"");
					s=s.replaceAll(capPlural, "\"+this.domain.getCapFirstPlural()+\"");
					s=s.replaceAll(lowerPlural, "\"+this.domain.getLowerFi\rstPlural()+\"");
					s=s.replaceAll(capDomainName, "\"+this.domain.getCapFirstDomainName()+\"");
					s=s.replaceAll(lowerDomainName, "\"+this.domain.getLowerFirstDomainName()+\"");
					sb.append(first.getText().trim()).append(serialInt).append("L,").append(countFrpmtSpaceTabs(s)).append(",\"").append(s.trim()).append(end.getText().trim()).append("\n");
					serialInt += 1000;
				}
				if (completeMethod.isSelected()) {
					sb.append("method.setMethodStatementList(WriteableUtil.merge(sList));\n");
					sb.append("return method;\n");
				}
				target.setText(sb.toString());
				JOptionPane.showMessageDialog(null,  "Congralations!","Ｓuccess", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private Integer countFrpmtSpaceTabs(String s) {
		Integer spaces = 0;
		for (int i=0;i<s.length();i++) {
			if (s.charAt(i)==' ') spaces += 1;
			else if (s.charAt(i)=='\t') spaces += 4;
			else break;
		}
		return (int)Math.ceil(spaces/4);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String [] args) {
		try{	
			java.awt.EventQueue.invokeLater(new Runnable() {

	            @Override
	            public void run() {
	            	try {
	            		new AdvancedTool().setVisible(true);
	            	} catch (Exception e){
	            		e.printStackTrace();
	            	}
	            }
	        });
		} catch (Exception e){
			JOptionPane.showMessageDialog(null, e.getMessage(), "信息", JOptionPane.ERROR_MESSAGE);
		}
	}
}
