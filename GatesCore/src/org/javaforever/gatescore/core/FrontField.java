package org.javaforever.gatescore.core;

import java.io.Serializable;

import org.javaforever.gatescore.utils.StringUtil;

public class FrontField implements Comparable<FrontField>,Cloneable,Serializable {
	private static final long serialVersionUID = -1782016933973123004L;
	protected long serial = 0L;
	protected String fieldName;
	protected String fieldComment;
	protected String fieldAlias;
	protected String fieldValue;
	protected String label;
	protected boolean fixed = false;
	protected FrontType fieldType;
	
	public FrontField() {
		super();
	}
	
	public FrontField(String fieldName, String typeString) {
		super();
		this.fieldName = fieldName;
		this.fieldType = new FrontType(typeString);
	}
	
	public String getAliasOrName() {
		if (!StringUtil.isBlank(this.fieldAlias)) return this.fieldAlias;
		else return this.fieldName;			
	}
	
	@Override
	public int compareTo(FrontField o) {
		return this.fieldName.compareTo(o.getFieldName());
	}
	
	public String getText(){
		if (this.label!= null && !this.label.equals("")) return this.label;
		else return this.fieldName;
	}
	
	public Object clone() {
		FrontField o = null;
		try {
			o = (FrontField) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return o;
	}
	
	public String getLowerFirstFieldName() {
		return StringUtil.lowerFirst(this.getFieldName());
	}
	
	public String getCapFirstFieldName() {
		return StringUtil.capFirst(this.getFieldName());
	}

	public long getSerial() {
		return serial;
	}

	public void setSerial(long serial) {
		this.serial = serial;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldComment() {
		return fieldComment;
	}

	public void setFieldComment(String fieldComment) {
		this.fieldComment = fieldComment;
	}

	public String getFieldAlias() {
		return fieldAlias;
	}

	public void setFieldAlias(String fieldAlias) {
		this.fieldAlias = fieldAlias;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isFixed() {
		return fixed;
	}

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public FrontType getFieldType() {
		return fieldType;
	}

	public void setFieldType(FrontType fieldType) {
		this.fieldType = fieldType;
	}
}
