package org.javaforever.gatescore.core;

import java.io.Serializable;

public abstract class FrontConfigFile implements Comparable<FrontConfigFile>,Cloneable,Serializable {
	private static final long serialVersionUID = -1437803357763369579L;
	protected String standardName;
	protected String folderPath;

	public abstract String generateConfigFileString() throws Exception;

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	@Override
	public int compareTo(FrontConfigFile cf) {
		return this.standardName.compareTo(cf.getStandardName());
	}

	public String getFolderPath() {
		return folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}
}
