package org.javaforever.gatescore.verb;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVerb;
import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;
import org.javaforever.gatescore.utils.StringUtil;

public class ListActive extends FrontVerb {
	private static final long serialVersionUID = -7040702252039560054L;

	public ListActive(FrontDomain d) {
		super(d);
		this.setVerbName("ListActive"+d.getCapFirstDomainName());
	}
	
	@Override
	public FrontMethod generateControllerMethod() throws Exception {
		FrontMethod method = new FrontMethod();
		method.setStandardName("listActive"+this.domain.getCapFirstPlural());
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,1,"listActive"+this.domain.getCapFirstPlural()+"() {"));
		sList.add(new Statement(2000L,1,"listActive"+this.domain.getCapFirstPlural()+"().then("));
		sList.add(new Statement(3000L,2,"response => {"));
		sList.add(new Statement(4000L,2,"this.active"+this.domain.getCapFirstPlural()+" = response.data.rows"));
		sList.add(new Statement(5000L,1,"})"));
		sList.add(new Statement(6000L,1,"},"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}

	@Override
	public FrontMethod generateApiMethod() throws Exception {
		FrontMethod method = new FrontMethod();
		method.setStandardName("listActive"+this.domain.getCapFirstPlural());
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"export function listActive"+this.domain.getCapFirstPlural()+"() {"));
		sList.add(new Statement(2000L,0,"return request({"));
		if (this.domain.isUseController()) {
			sList.add(new Statement(3000L,1,"url: '"+this.domain.getLowerFirstDomainName()+"Controller/listActive"+this.domain.getCapFirstPlural()+"',"));
		} else {
			sList.add(new Statement(3000L,1,"url: '"+this.domain.getLowerFirstDomainName()+"Facade/listActive"+this.domain.getCapFirstPlural()+"',"));
		}
		sList.add(new Statement(4000L,1,"method: 'post'"));
		sList.add(new Statement(5000L,0,"})"));
		sList.add(new Statement(6000L,0,"}"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;

	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		//TODO
		return null;
	}


}
