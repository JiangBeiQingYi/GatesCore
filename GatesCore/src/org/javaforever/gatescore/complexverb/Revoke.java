package org.javaforever.gatescore.complexverb;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontManyToMany;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;
import org.javaforever.gatescore.utils.StringUtil;

public class Revoke extends FrontTwinsVerb{
	private static final long serialVersionUID = -3541760626001288740L;
	
	public Revoke(FrontManyToMany mtm) {
		super();
		this.master = mtm.getMaster();
		this.slave = mtm.getSlave();
		this.verbName = "Revoke"+this.slave.getAliasOrNamePlural()+"From"+this.master.getCapFirstDomainName();
	}

	@Override
	public FrontMethod generateControllerMethod() throws Exception {
		FrontMethod method = new FrontMethod();
		method.setStandardName("revoke"+this.slave.getAliasOrNamePlural()+"From"+this.master.getCapFirstDomainName());
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,1,"revoke"+this.slave.getAliasOrNamePlural()+"From"+this.master.getCapFirstDomainName()+"("+this.master.getLowerFirstDomainName()+"Id,"+StringUtil.lowerFirst(this.slave.getAliasOrName())+"Ids) {"));
		sList.add(new Statement(2000L,1,"revoke"+this.slave.getAliasOrNamePlural()+"From"+this.master.getCapFirstDomainName()+"("+this.master.getLowerFirstDomainName()+"Id,"+StringUtil.lowerFirst(this.slave.getAliasOrName())+"Ids).then(response => {this.listAvailableActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id("+this.master.getLowerFirstDomainName()+"Id)})"));
		sList.add(new Statement(3000L,1,"},"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}

	@Override
	public FrontMethod generateApiMethod() throws Exception {
		FrontMethod method = new FrontMethod();
		method.setStandardName("revoke"+this.slave.getAliasOrNamePlural()+"From"+this.master.getCapFirstDomainName());
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"export function revoke"+this.slave.getAliasOrNamePlural()+"From"+this.master.getCapFirstDomainName()+"("+this.master.getLowerFirstDomainName()+"Id,"+StringUtil.lowerFirst(this.slave.getAliasOrName())+"Ids){"));
		sList.add(new Statement(2000L,0,"return request({"));
		if (this.master.isUseController()) {
			sList.add(new Statement(3000L,1,"url:'"+this.master.getLowerFirstDomainName()+"Controller/revoke"+this.slave.getAliasOrNamePlural()+"From"+this.master.getCapFirstDomainName()+"',"));
		} else {
			sList.add(new Statement(3000L,1,"url:'"+this.master.getLowerFirstDomainName()+"Facade/revoke"+this.slave.getAliasOrNamePlural()+"From"+this.master.getCapFirstDomainName()+"',"));
		}
		sList.add(new Statement(4000L,1,"method:'post',"));
		sList.add(new Statement(5000L,1,"params:{"+this.master.getLowerFirstDomainName()+"Id:"+this.master.getLowerFirstDomainName()+"Id,"+StringUtil.lowerFirst(this.slave.getAliasOrName())+"Ids:"+StringUtil.lowerFirst(this.slave.getAliasOrName())+"Ids}"));
		sList.add(new Statement(6000L,0,"})"));
		sList.add(new Statement(7000L,0,"}"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;

	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		// TODO
		return null;
	}

}
