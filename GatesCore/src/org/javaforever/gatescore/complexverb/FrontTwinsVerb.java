package org.javaforever.gatescore.complexverb;

import java.io.Serializable;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontManyToMany;
import org.javaforever.gatescore.core.FrontMethod;

public abstract class FrontTwinsVerb implements Comparable<FrontTwinsVerb>,Cloneable,Serializable{
	private static final long serialVersionUID = -647084255028990460L;
	protected FrontDomain master;
	protected FrontDomain slave;
	protected String label;
	protected String verbName;
	
	public abstract FrontMethod generateControllerMethod() throws Exception;
	public abstract FrontMethod generateApiMethod() throws Exception;
	public abstract FrontCodeBlock generateRouteBlockBlock() throws Exception;
	
	public FrontDomain getMaster() {
		return master;
	}
	public void setMaster(FrontDomain master) {
		this.master = master;
	}
	public FrontDomain getSlave() {
		return slave;
	}
	public void setSlave(FrontDomain slave) {
		this.slave = slave;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getText(){
		if (this.label!= null && !this.label.equals("")) return this.label;
		else return this.verbName;
	}
	public String getVerbName() {
		return verbName;
	}
	public void setVerbName(String verbName) {
		this.verbName = verbName;
	}
	
	@Override
	public int compareTo(FrontTwinsVerb o) {
		return this.getVerbName().compareTo(o.getVerbName());
	}
	
	public void setSlaveAlias(String slaveAlias){
		this.slave.setAlias(slaveAlias);
	}
}
