package org.javaforever.gatescore.complexverb;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontManyToMany;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;

public class ListMyActive extends FrontTwinsVerb{
	private static final long serialVersionUID = 6427439293045904307L;
	
	public ListMyActive(FrontManyToMany mtm) {
		super();
		this.master = mtm.getMaster();
		this.slave = mtm.getSlave();
		this.verbName = "ListActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id";
	}

	@Override
	public FrontMethod generateControllerMethod() throws Exception {
		FrontMethod method = new FrontMethod();
		method.setStandardName("listActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id");
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,1,"listActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id("+this.master.getLowerFirstDomainName()+"Id) {"));
		sList.add(new Statement(2000L,1,"listActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id("+this.master.getLowerFirstDomainName()+"Id).then("));
		sList.add(new Statement(3000L,2,"response => {"));
		sList.add(new Statement(4000L,2,"this.active"+this.slave.getAliasOrNamePlural()+" = response.data.rows"));
		sList.add(new Statement(5000L,1,"})"));
		sList.add(new Statement(6000L,1,"},"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}

	@Override
	public FrontMethod generateApiMethod() throws Exception {
		FrontMethod method = new FrontMethod();
		method.setStandardName("listActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id");
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"export function listActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id("+this.master.getLowerFirstDomainName()+"Id){"));
		sList.add(new Statement(2000L,0,"return request({"));
		if (this.master.isUseController()) {
			sList.add(new Statement(3000L,1,"url:'"+this.master.getLowerFirstDomainName()+"Controller/listActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id',"));
		} else {
			sList.add(new Statement(3000L,1,"url:'"+this.master.getLowerFirstDomainName()+"Facade/listActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id',"));
		}
		sList.add(new Statement(4000L,1,"method:'post',"));
		sList.add(new Statement(5000L,1,"params:{"+this.master.getLowerFirstDomainName()+"Id:"+this.master.getLowerFirstDomainName()+"Id}"));
		sList.add(new Statement(6000L,0,"})"));
		sList.add(new Statement(7000L,0,"}"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;

	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
